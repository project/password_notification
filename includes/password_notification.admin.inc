<?php

/**
 * @file
 * Menu callback include file for admin/settings pages.
 */

/**
 * Implementation of menu callback.
 */
function password_notification_admin_form($form, &$form_state) {
  $form = array();
  $body_content = "Dear [user-name], \n\n  Your Password has been changed by [current-user-name]. \n\n [site-url]";
  $form['password_notification_mail'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Password Notification Email'),
    '#description' => t('Set whether Password Notification Email is active or not.'),
    '#default_value' => variable_get('password_notification_mail', 0),
  );
  $form['password_notification_log'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Log History'),
    '#description' => t('Set whether Password Notification Logs Entry is active or not.'),
    '#default_value' => variable_get('password_notification_log', 0),
  );
  $form['password_notification_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Password Notification Mail Content'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['password_notification_fieldset']['mail_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t("The mail's subject."),
    '#required' => TRUE,
    '#default_value' => variable_get('mail_subject', 'Password Changed'),
  );
  $form['password_notification_fieldset']['mail_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#description' => t("The mail's message body.<br/> Available Tokens: [user-name], [current-user-name], [site-url], [site-name]"),
    '#required' => TRUE,
    '#default_value' => variable_get('mail_body', $body_content),
  );
  $form['password_notification_fieldset']['mail_bcc'] = array(
    '#type' => 'textarea',
    '#title' => t('Bcc'),
    '#description' => t('Please Provide Bcc with comma separated.<br/> Example: user1@test.com,user2@test.com'),
    '#default_value' => variable_get('mail_bcc', ''),
  );
  $form['password_notification_fieldset']['mail_cc'] = array(
    '#type' => 'textarea',
    '#title' => t('Cc'),
    '#description' => t('Please Provide Cc with comma separated.<br/> Example: user1@test.com,user2@test.com'),
    '#default_value' => variable_get('mail_cc', ''),
  );
  $form['delete_history_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delete Limit for Password Changed History Table'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $limits = drupal_map_assoc(array(1000, 2000, 3000, 4000, 5000));
  $form['delete_history_fieldset']['delete_records_limit'] = array(
    '#title' => t('Limit'),
    '#type' => 'select',
    '#default_value' => variable_get('delete_records_limit', 1000),
    '#required' => TRUE,
    '#description' => 'Select the Limit to Delete Records using Batch Operation in Delete History Page.',
    '#options' => $limits,
  );
  return system_settings_form($form);
}
