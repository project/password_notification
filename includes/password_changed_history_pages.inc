<?php

/**
 * @file
 * The Password Changed history page-related code.
 */

/**
 * Page callback for the Password Changed history page.
 *
 * @param object $account
 *   Comment about this variable.
 *
 * @return string
 *   A string of html to be printed.
 */
function password_changed_history_form($form, &$form_state, $account = NULL) {
  $form = array();
  $date_format = 'The date format should be DD-MM-YYYY and Example Format is ' . date('d-m-Y');
  $collapsed = TRUE;
  $username = $email = $start_date = $end_date = '';
  // Configure the table header columns.
  $header = array(
    array('data' => 'Name', 'field' => 'username'),
    array('data' => 'Email', 'field' => 'email'),
    array('data' => 'User Id', 'field' => 'user_id'),
    array('data' => 'Changed by(User Id)', 'field' => 'changed_by_user_id'),
    array('data' => 'Changed by(Name)', 'field' => 'changed_by_user_name'),
    array('data' => 'IP Address', 'field' => 'ip_address'),
    array('data' => 'Mail Sent', 'field' => 'mail_send'),
    array('data' => 'Date', 'field' => 'created', 'sort' => 'desc'),
  );
  // Select Query from Password Changed History Table.
  $query = db_select('password_changed_history', 'p')
    ->extend('PagerDefault')
    ->extend('TableSort');
  if ($account) {
    $query->condition('p.user_id', $account->uid, '=');
  }
  // Filter by Username.
  if (!empty($_GET['username'])) {
    $username = $_GET['username'];
    $query->condition('p.username', '%' . db_like($_GET['username']) . '%', 'LIKE');
  }
  // Filter by Email.
  if (!empty($_GET['email'])) {
    $email = $_GET['email'];
    $query->condition('p.email', $_GET['email'], '=');
  }
  if (!empty($_GET['start_date']) && !empty($_GET['end_date'])) {
    $start_date = $_GET['start_date'];
    $start_date_timestamp = strtotime($start_date . '00:00:00');
    $end_date = $_GET['end_date'];
    $end_date_timestamp = strtotime($end_date . '23:59:59');
    $query->condition('p.created', array($start_date_timestamp, $end_date_timestamp), 'BETWEEN');
  }
  $query->fields('p', array('username',
    'email',
    'user_id',
    'changed_by_user_id',
    'changed_by_user_name',
    'ip_address',
    'mail_send',
    'created',
  ))
    ->limit(10)
    ->orderByHeader($header);

  $results = $query->execute();
  if (!empty($results)) {
    $rows = array();
    foreach ($results as $row) {
      $rows[] = array($row->username,
        $row->email,
        $row->user_id,
        $row->changed_by_user_id,
        $row->changed_by_user_name,
        $row->ip_address,
        $row->mail_send,
        date('d-m-Y h:i A', $row->created),
      );
    }
    /* Filters Code Start */
    if (!empty($_GET['username']) || !empty($_GET['email']) || !empty($_GET['start_date']) || !empty($_GET['end_date'])) {
      $collapsed = FALSE;
    }
    $form['filter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Filter option'),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
    );
    if (!$account) {
      $form['filter']['username'] = array(
        '#type' => 'textfield',
        '#title' => t('Username'),
        '#default_value' => $username,
        '#size' => 20,
      );
      $form['filter']['email'] = array(
        '#type' => 'textfield',
        '#title' => t('Email'),
        '#default_value' => $email,
        '#size' => 20,
      );
    }
    $form['filter']['start_date'] = array(
      '#type' => 'textfield',
      '#title' => t('Start Date'),
      '#description' => $date_format,
      '#default_value' => $start_date,
      '#size' => 20,
    );
    $form['filter']['end_date'] = array(
      '#type' => 'textfield',
      '#title' => t('End Date'),
      '#description' => $date_format,
      '#default_value' => $end_date,
      '#size' => 20,
    );
    $form['filter']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Apply'),
    );
    $form['filter']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
      '#submit' =>
    array('reset_form_submit'),
      '#limit_validation_errors' => array(),
    );
    /* Filters Code end */
    $form['table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No Password Changed history available.'),
    );
    $form['pager'] = array('#markup' => theme('pager'));
  }
  return $form;
}

/**
 * Implements using hook_form_validate().
 */
function password_changed_history_form_validate($form, &$form_state) {

  // Email Validation.
  if (isset($form_state['values']['email']) && !empty($form_state['values']['email'])) {
    if (!valid_email_address($form_state['values']['email'])) {
      form_set_error('email', 'Please Provide Valid Email Address.');
    }
  }
  /* Date Filter Validation Code Start */
  // Start Date Validation.
  if (isset($form_state['values']['end_date']) && !empty($form_state['values']['end_date'])) {
    if (!$form_state['values']['start_date']) {
      form_set_error('start_date', 'Please Provide Start Date.');
    }
  }
  // End Date Validation.
  if (isset($form_state['values']['start_date']) && !empty($form_state['values']['start_date'])) {
    if (!$form_state['values']['end_date']) {
      form_set_error('end_date', 'Please Provide End Date.');
    }
  }
  /* Date Filter Validation Code End */
}

/**
 * Implements using hook_form_submit().
 */
function password_changed_history_form_submit($form, &$form_state) {
  $username = $email = $start_date = $end_date = '';
  $current_path = current_path();
  // Username.
  if (isset($form_state['values']['username']) && !empty($form_state['values']['username'])) {
    $username = $form_state['values']['username'];
  }
  // Email.
  if (isset($form_state['values']['email']) && !empty($form_state['values']['email'])) {
    $email = $form_state['values']['email'];
  }
  // Start date.
  if (isset($form_state['values']['start_date']) && !empty($form_state['values']['start_date'])) {
    $start_date = $form_state['values']['start_date'];
  }
  // End date.
  if (isset($form_state['values']['end_date']) && !empty($form_state['values']['end_date'])) {
    $end_date = $form_state['values']['end_date'];
  }
  if (!empty($username) || !empty($email) || !empty($start_date) || !empty($end_date)) {
    $form_state['redirect'] = array($current_path, array(
      'query' => array(
        'username' => $username,
        'email' => $email,
        'start_date' => $start_date,
        'end_date' => $end_date,
      ),
    ),
    );
  }
}

/**
 * Reset Button Link Callback.
 */
function reset_form_submit($form, &$form_state) {
  $current_path = current_path();
  $form_state['redirect'] = array($current_path);
}
