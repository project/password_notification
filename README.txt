Password Notification Module helps to Send the Password Notification Mail. to User When Update the User Password and also Log the Password Update History.

FEATURES
--------
a) Send Mail to Respective User when someone changes the password.
b) Maintain Log History User-Based and Globally(Administrator Only see the Entire History).
c) Remove Log History Using Batch Operation. (Administrator Only Handles the Operation).

INSTALLATION
------------
1. Copy the entire Password Notification directory to your contributed modules directory.
2. Log in as an administrator. Enable the module at admin/modules.

CONFIGURATION
-------------
Go to the Password Notification Configuration (Configuration >> People >> Password Notification).
  Path: /admin/config/people/password-notification
  a) Set whether Password Notification Email is active or not.
  b) Set whether Password Notification Logs Entry is active or not.
  c) Configure the Password Notification Mail Content with Subject, Message, Bcc, and Cc.
  d) Set the Limit to Delete Records using Batch Operation in Delete History Page.

PASSWORD CHANGED HISTORY PAGE URLS
----------------------------------
1) All History Page (People >> Password Changed History)
     Url - /admin/people/password-changed/history  (Administrator Only see the Entire History).
2) User-Based History Page
     Url - /user/[uid]/password-changed/history (Authenticated User Only see the Own History).
3) Delete History Page (People >> Password Changed History >> Delete History)
    Url - /admin/people/password-changed/history/delete (Administrator Only Handles the Operation).
